#include <stdio.h>


int greatest_common(int num_1, int num_2)
{
    if ( num_2 > num_1) 
    {   
        int temp = num_2;
        num_2 = num_1;
        num_1 = temp;
    }
    
    for (int i = num_2; i >=  1; i--) 
    {
        if (num_1 % i == 0 && num_2 % i ==0) 
        {
            return i;
        }
    }
}
int main() {

    printf("Greatest commmon factor: %d\n", greatest_common(2,2));
    return 0;
}