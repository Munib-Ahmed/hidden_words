#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define ROW_SIZE        15
#define COLUMN_SIZE     15
#define WORDS_SIZE      16

char puzzle[ROW_SIZE][COLUMN_SIZE] = {
    {'a', 'x', 'd', 'u', 'r', 'y', 'h', 'p', 's', 'p', 't', 't', 'i', 'd', 't'},
    {'v', 'n', 'x', 's', 'e', 'c', 'a', 'q', 'c', 'u', 'i', 'u', 'o', 'z', 'p'},
    {'m', 'e', 'r', 'g', 'f', 'w', 'o', 'z', 'i', 's', 'n', 'l', 'k', 'a', 'c'},
    {'v', 's', 'c', 'x', 's', 'e', 's', 'x', 'o', 'f', 'l', 'w', 'z', 'j', 'z'},
    {'v', 't', 'r', 'p', 'n', 'd', 'l', 'p', 'b', 'a', 'c', 'h', 'e', 'b', 'y'},
    {'p', 'r', 'e', 'k', 'a', 'w', 'e', 'c', 'r', 'c', 't', 'x', 'n', 's', 'b'},
    {'f', 'p', 'd', 'm', 'r', 'd', 'l', 'o', 's', 'p', 'e', 'j', 'a', 'q', 't'},
    {'w', 'h', 'i', 'n', 't', 'l', 'y', 'i', 'b', 't', 'c', 'a', 's', 'h', 'n'},
    {'l', 'a', 't', 'c', 'i', 'q', 'n', 'n', 'r', 'i', 'l', 'h', 't', 'y', 'j'},
    {'k', 'r', 'r', 'x', 'o', 'i', 'f', 'k', 'y', 'b', 'w', 'o', 'e', 'w', 'a'},
    {'v', 'c', 'p', 'd', 'u', 'y', 'z', 'k', 'l', 'e', 't', 'n', 'a', 'k', 'w'},
    {'i', 'u', 'e', 'o', 'h', 'k', 'n', 'a', 'b', 'd', 'o', 't', 'a', 'n', 'a'},
    {'f', 'a', 'q', 'h', 'y', 't', 'j', 'b', 'i', 'm', 'y', 'm', 'u', 'd', 's'},
    {'v', 'q', 'z', 'f', 'c', 'j', 'i', 'e', 'e', 'm', 'n', 'k', 'u', 'x', 'l'},
    {'b', 'a', 'l', 'a', 'n', 'c', 'e', 'w', 'e', 'c', 'n', 'u', 'o', 'p', 'b'}
};

char puzzle_solved[ROW_SIZE][COLUMN_SIZE] = {
    {'a', 'x', 'd', 'u', 'r', 'y', 'h', 'p', 's', 'p', 't', 't', 'i', 'd', 't'},
    {'v', 'n', 'x', 's', 'e', 'c', 'a', 'q', 'c', 'u', 'i', 'u', 'o', 'z', 'p'},
    {'m', 'e', 'r', 'g', 'f', 'w', 'o', 'z', 'i', 's', 'n', 'l', 'k', 'a', 'c'},
    {'v', 's', 'c', 'x', 's', 'e', 's', 'x', 'o', 'f', 'l', 'w', 'z', 'j', 'z'},
    {'v', 't', 'r', 'p', 'n', 'd', 'l', 'p', 'b', 'a', 'c', 'h', 'e', 'b', 'y'},
    {'p', 'r', 'e', 'k', 'a', 'w', 'e', 'c', 'r', 'c', 't', 'x', 'n', 's', 'b'},
    {'f', 'p', 'd', 'm', 'r', 'd', 'l', 'o', 's', 'p', 'e', 'j', 'a', 'q', 't'},
    {'w', 'h', 'i', 'n', 't', 'l', 'y', 'i', 'b', 't', 'c', 'a', 's', 'h', 'n'},
    {'l', 'a', 't', 'c', 'i', 'q', 'n', 'n', 'r', 'i', 'l', 'h', 't', 'y', 'j'},
    {'k', 'r', 'r', 'x', 'o', 'i', 'f', 'k', 'y', 'b', 'w', 'o', 'e', 'w', 'a'},
    {'v', 'c', 'p', 'd', 'u', 'y', 'z', 'k', 'l', 'e', 't', 'n', 'a', 'k', 'w'},
    {'i', 'u', 'e', 'o', 'h', 'k', 'n', 'a', 'b', 'd', 'o', 't', 'a', 'n', 'a'},
    {'f', 'a', 'q', 'h', 'y', 't', 'j', 'b', 'i', 'm', 'y', 'm', 'u', 'd', 's'},
    {'v', 'q', 'z', 'f', 'c', 'j', 'i', 'e', 'e', 'm', 'n', 'k', 'u', 'x', 'l'},
    {'b', 'a', 'l', 'a', 'n', 'c', 'e', 'w', 'e', 'c', 'n', 'u', 'o', 'p', 'b'}
};

const char *words[WORDS_SIZE] = {
    "balance",
    "bank",
    "cash",
    "check",
    "coin",
    "credit",
    "debit",
    "deposit",
    "dollar",
    "loan",
    "money",
    "paws",
    "pounce",
    "sunwest",
    "transfer",
    "withdraw"
};

void display_words()
{
    for(int i=0;i<WORDS_SIZE;i++)
    {
        printf("    %s  \n", words[i]);
    }
}

void display_pattern()
{
    for(int i=0;i<ROW_SIZE;i++)
    {
        for(int j=0;j<COLUMN_SIZE;j++)
        {
            printf("%c  ", puzzle_solved[i][j]);
        }
        printf("\n");
    }    
}

void rev_str(char *str1)  
{  
    int i, len, temp;  
    len = strlen(str1);
        
    for (i = 0; i < len/2; i++)  
    {  
        temp = str1[i];  
        str1[i] = str1[len - i - 1];  
        str1[len - i - 1] = temp;  
    }  
}  

int check_str_index(char* str, char* substr)
{ 
    bool isPresent = false;
    int i=0;
    int j=0;
    for (i = 0; str[i] != '\0'; i++) {
        isPresent = false;
        for (j = 0; substr[j] != '\0'; j++) {
            if (str[i + j] != substr[j]) {
                isPresent = false;
                break;
            }
            isPresent = true;
        }
        if (isPresent) {
            break;
        }
    }
     
    if (isPresent) {
        return i;
    }
    return -1;
}

void row_capitalize_puzzle(int row, int column, int str_length)
{
    for(int i=0;i<str_length;i++)
    {
        puzzle_solved[row][column+i] = puzzle[row][column+i] - 32;
    }
}

void digonal_1_capitalize_puzzle(int row, int column, int str_length)
{
    for(int i=0;i<str_length;i++)
    {
        puzzle_solved[column+i][column+row+i] = puzzle[column+i][column+row+i] - 32;
    }
}

void digonal_1_capitalize_puzzle_2(int row, int column, int str_length)
{
    for(int i=0;i<str_length;i++)
    {
        puzzle_solved[column+row+i][column+i] = puzzle[column+row+i][column+i] - 32;
    }
}

void digonal_2_capitalize_puzzle(int row, int column, int str_length)
{
    for(int i=0;i<str_length;i++)
    {
        puzzle_solved[column-i][row+i] = puzzle[column-i][row+i] - 32;
    }
}

void digonal_2_capitalize_puzzle_2(int row, int column, int str_length)
{
    for(int i=0;i<str_length;i++)
    {
        puzzle_solved[row+i][column-i] = puzzle[row+i][column-i] - 32;
    }
}

void column_capitalize_puzzle(int row, int column, int str_length)
{
    for(int i=0;i<str_length;i++)
    {
        puzzle_solved[row+i][column] = puzzle[row+i][column] - 32;
    }
}

void row_wise_check()
{
    int str_length=0;
    int str_index=-1;
    char puzzle_str[COLUMN_SIZE*2];
    char word[COLUMN_SIZE];
    char inverted_word[COLUMN_SIZE];

    for(int i=0;i<ROW_SIZE;i++)
    {
        for(int j=0;j<COLUMN_SIZE;j++)
        {
            puzzle_str[j]=puzzle[i][j];
        }
        for (int k=0;k<WORDS_SIZE;k++)
        {
            strcpy(word, words[k]);
            str_index = check_str_index(puzzle_str, word);
            if(str_index!=-1)
            {
                row_capitalize_puzzle(i,str_index,strlen(word));
            }
            strcpy(inverted_word, word);
            rev_str(inverted_word);
            str_index = check_str_index(puzzle_str, inverted_word);
            if(str_index!=-1)
            {
                row_capitalize_puzzle(i,str_index,strlen(word));
            }
        }
    }  
}

void column_wise_check()
{
    int str_length=0;
    int str_index=-1;
    char puzzle_str[ROW_SIZE*2];
    char word[ROW_SIZE];
    char inverted_word[ROW_SIZE];

    for(int i=0;i<COLUMN_SIZE;i++)
    {
        for(int j=0;j<ROW_SIZE;j++)
        {
            puzzle_str[j]=puzzle[j][i];
        }
        for (int k=0;k<WORDS_SIZE;k++)
        {
            strcpy(word, words[k]);
            str_index = check_str_index(puzzle_str, word);
            if(str_index!=-1)
            {
                column_capitalize_puzzle(str_index,i,strlen(word));
            }
            strcpy(inverted_word, word);
            rev_str(inverted_word);
            str_index = check_str_index(puzzle_str, inverted_word);
            if(str_index!=-1)
            {
                column_capitalize_puzzle(str_index,i,strlen(word));
            }
        }
    }  
}

void digonal_1_check()
{
    int str_length=0;
    int str_index=-1;
    char puzzle_str[COLUMN_SIZE*2];
    char word[COLUMN_SIZE];
    char inverted_word[COLUMN_SIZE];

    int i_inc=0;
    int j_inc=0;
    for(int i=0;i<ROW_SIZE;i++)
    {
        memset(puzzle_str, '\0', sizeof(puzzle_str));
        i_inc=0;
        j_inc=i;
        for(int j=0;j<COLUMN_SIZE;j++)
        {
            if(i_inc<ROW_SIZE && j_inc<COLUMN_SIZE)
            {
                puzzle_str[i_inc]=puzzle[i_inc][j_inc];
            }
            i_inc++;
            j_inc++;
        }
        for (int k=0;k<WORDS_SIZE;k++)
        {
            strcpy(word, words[k]);
            str_index = check_str_index(puzzle_str, word);
            if(str_index!=-1)
            {
                digonal_1_capitalize_puzzle(i,str_index,strlen(word));
            }
            strcpy(inverted_word, word);
            rev_str(inverted_word);
            str_index = check_str_index(puzzle_str, inverted_word);
            if(str_index!=-1)
            {
                digonal_1_capitalize_puzzle(i,str_index,strlen(word));
            }
        }
    }
    for(int i=0;i<ROW_SIZE;i++)
    {
        memset(puzzle_str, '\0', sizeof(puzzle_str));
        i_inc=i;
        j_inc=0;
        for(int j=0;j<COLUMN_SIZE;j++)
        {
            if(i_inc<ROW_SIZE && j_inc<COLUMN_SIZE)
            {
                puzzle_str[j_inc]=puzzle[i_inc][j_inc];
            }
            i_inc++;
            j_inc++;
        }
        for (int k=0;k<WORDS_SIZE;k++)
        {
            strcpy(word, words[k]);
            str_index = check_str_index(puzzle_str, word);
            if(str_index!=-1)
            {
                digonal_1_capitalize_puzzle_2(i,str_index,strlen(word));
            }
            strcpy(inverted_word, word);
            rev_str(inverted_word);
            str_index = check_str_index(puzzle_str, inverted_word);
            if(str_index!=-1)
            {
                digonal_1_capitalize_puzzle_2(i,str_index,strlen(word));
            }
        }
    } 
}

void digonal_2_check()
{
    int str_length=0;
    int str_index=-1;
    char puzzle_str[COLUMN_SIZE*2];
    char word[COLUMN_SIZE];
    char inverted_word[COLUMN_SIZE];

    int i_inc=0;
    int j_inc=0;
    int index=0;
    for(int i=ROW_SIZE-1;i>=0;i--)
    {
        memset(puzzle_str, '\0', sizeof(puzzle_str));
        i_inc=0;
        j_inc=i;
        for(int j=0;j<COLUMN_SIZE;j++)
        {
            if(i_inc<ROW_SIZE && j_inc<COLUMN_SIZE)
            {
                puzzle_str[i_inc]=puzzle[j_inc][i_inc];
            }
            i_inc++;
            j_inc--;
        }
        for (int k=0;k<WORDS_SIZE;k++)
        {
            strcpy(word, words[k]);
            str_index = check_str_index(puzzle_str, word);
            if(str_index!=-1)
            {
                digonal_2_capitalize_puzzle(str_index,i-str_index,strlen(word));
            }
            strcpy(inverted_word, word);
            rev_str(inverted_word);
            str_index = check_str_index(puzzle_str, inverted_word);
            if(str_index!=-1)
            {
                digonal_2_capitalize_puzzle(str_index,i-str_index,strlen(word));
            }
        }
    }
    for(int i=0;i<ROW_SIZE;i++)
    {
        memset(puzzle_str, '\0', sizeof(puzzle_str));
        i_inc=i;
        j_inc=COLUMN_SIZE-1;
        for(int j=0;j<COLUMN_SIZE;j++)
        {
            if(i_inc<ROW_SIZE && i_inc>=0 && j_inc>=0 && j_inc<COLUMN_SIZE)
            {
                puzzle_str[j]=puzzle[i_inc][j_inc];
            }
            i_inc++;
            j_inc--;
        }
        for (int k=0;k<WORDS_SIZE;k++)
        {
            strcpy(word, words[k]);
            str_index = check_str_index(puzzle_str, word);
            if(str_index!=-1)
            {
                digonal_2_capitalize_puzzle_2(i+str_index,COLUMN_SIZE-str_index-1,strlen(word));
            }
            strcpy(inverted_word, word);
            rev_str(inverted_word);
            str_index = check_str_index(puzzle_str, inverted_word);
            if(str_index!=-1)
            {
                digonal_2_capitalize_puzzle_2(i+str_index,COLUMN_SIZE-str_index-1,strlen(word));
            }
        }
    } 
}

int main() {
    printf("This code will only work properly for Square matrix, in other cases it may not detect all the hidden words");
    printf("Original pattern\n");
    display_pattern();
    printf("\nHidden Words\n");
    display_words();

    row_wise_check();
    column_wise_check();
    digonal_1_check();
    digonal_2_check();

    printf("\n\nPattern after finding the hidden words\n");
    display_pattern();
    return 0;
}